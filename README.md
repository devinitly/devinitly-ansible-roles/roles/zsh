ZSH
=========

This role installs zsh and configures the shell with Oh-my-zsh and powerline10k

Requirements
------------

- Ansible
- wget
- git

Role Variables
--------------

The rule uses a single variable to define which system user to install for:
- owner: $USER

The owner field can be overridden by defining the varible inline or using the variables main.yml

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
---
- hosts: localhost
  connection: local
  gather_facts: yes
  vars:
    ansible_python_interpreter: /usr/bin/python3
  roles:
    - zsh

License
-------

BSD
